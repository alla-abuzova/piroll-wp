<?php


class About_Us_Widget extends WP_Widget
{

    function __construct() {
        // Запускаем родительский класс
        parent::__construct(
            'id_about_us', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: my_widget
            'About Us',
            array('description' => 'This widget enter a data about company into site front')
        );
    }

    public function widget( $args, $instance ) {

        $title = apply_filters( 'widget_title', $instance['title'] );
        $description = $instance['description'];
        $img_logo = $instance['img_logo'];

        echo $args['before_widget'];

        if( $description ){
            //echo $args['before_title'] . $title . $args['after_title'];

            ?>

            <section class="about-us" id="about-us">
                <div class="container">
                    <p class="about-us-title"><?php echo $title ?></p>
                    <p class="about-us-description"><?php echo $description ?></p>
                    <!--<img src="http://piroll-wp/wp-content/themes/twentytwenty-child/assets/images/signature.png" alt=" ">-->
                    <img src="<?php echo $img_logo ?>" alt=" ">
                </div>
            </section>

            <?php
        }
        echo $args['after_widget'];
    }

    public function form( $instance ) {
        /*global $title;
        global $description;
        global $img_logo;*/
        try{
            if ( isset( $instance[ 'title' ] ) ) {
                $title = $instance[ 'title' ];
            }
        } catch (Exception $e){
            echo "Ошибка title" . $e;
        }

        if ( isset( $instance[ 'description' ] ) ) {
            $description = $instance[ 'description' ];
        } else {
           // $description = "Пустое поле";
        }
        if ( isset( $instance[ 'img_logo' ] ) ) {
            $img_logo = $instance[ 'img_logo' ];
        } else {
           // $img_logo = "Пустое поле";
        }

        ?>

        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Заголовок</label>
            <input class="widefat" placeholder="Пустое поле" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <lable for="<?php echo $this->get_field_id('description');?>">Описание</lable>
            <input class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" type="text" value="<?php echo esc_attr( $description ); ?>" />
        </p>
        <p>
            <lable for="<?php echo $this->get_field_id('img_logo');?>">Описание</lable>
            <input class="widefat" id="<?php echo $this->get_field_id('img_logo'); ?>" name="<?php echo $this->get_field_name('img_logo'); ?>" type="text" value="<?php echo esc_attr( $img_logo ); ?>" />
        </p>
        <?php

    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
        $instance['img_logo'] = ( ! empty( $new_instance['img_logo'] ) ) ? strip_tags( $new_instance['img_logo'] ) : '';
        return $instance;
    }

}

function register_base_widget() {
    register_widget( 'About_Us_Widget' );
}
add_action( 'widgets_init', 'register_base_widget' );