<?php

add_action('admin_print_footer_script', 'my_action_javascript', 99);
function my_action_javascript(){
    ?>
    <script>
        JQuery(document).ready(function($) {
            var data = {
                action: 'my_action',
                whatever: 1234
            };

            JQuery.post(ajaxurl, data, function(response){
                alert('Получено с сервера: ' + response);
            });
        });
    </script>
<?php
}
?>