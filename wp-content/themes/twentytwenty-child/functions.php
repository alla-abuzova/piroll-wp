<?php
/*This file is part of twentytwenty-child, twentytwenty child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

if ( ! function_exists( 'suffice_child_enqueue_child_styles' ) ) {
	function twentytwenty_child_enqueue_child_styles() {
	    // loading parent style
	  /* wp_register_style(
	      'parente2-style',
	      get_template_directory_uri() . '/style.css'
	    );*/

	   // wp_enqueue_style( 'parente2-style' );
	    // loading child style
	    wp_register_style(
	      'childe2-style',
	      get_stylesheet_directory_uri() . '/style.css'
	    );
	    wp_enqueue_style( 'childe2-style');
	 }
}
add_action( 'wp_enqueue_scripts', 'twentytwenty_child_enqueue_child_styles' );

/*Write here your own functions */

/* Подключение стилей и скриптов*/

add_action( 'wp_enqueue_scripts', 'theme_add_scripts' );
function theme_add_scripts() {
    wp_deregister_script( 'twentytwenty-js' );


    // подключаем файл стилей темы
    wp_enqueue_style( 'docs_theme_min', get_stylesheet_directory_uri() .'/assets/css/docs.theme.min.css', array(), '1.0', true  );
    wp_enqueue_style( 'owl_carousel_min', get_stylesheet_directory_uri() .'/assets/css/owl.carousel.min.css', array(), '1.0', true  );
    wp_enqueue_style( 'owl_theme_default_min', get_stylesheet_directory_uri() .'/assets/css/owl.theme.default.min.css', array(), '1.0', true  );
    wp_enqueue_style( 'adding_style', get_stylesheet_directory_uri() .'/assets/css/adding_style.css', array(), '1.0', true  );
    // fontawesome
    wp_enqueue_style( 'fontawesome_css', get_stylesheet_directory_uri() .'/assets/fonts/fontawesome.css', array(), '1.0', true  );

    // подключаем js файл темы
   // wp_enqueue_script( 'jquery_min', 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', array(), '1.0', true );

    wp_enqueue_script( 'fontawesome_js', get_stylesheet_directory_uri() .'/assets/fonts/all.js', array(), '1.0', true );

    //if lt IE 9
    wp_enqueue_script( 'html5shiv', 'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', array(), '1.0', true );
    wp_enqueue_script( 'respond_min', 'https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js', array(), '1.0', true );

    wp_enqueue_script( 'owl_carousel_min', get_stylesheet_directory_uri() .'/assets/js/owl.carousel.min.js', array(), '1.0', true );
    wp_enqueue_script( 'highlight', get_stylesheet_directory_uri() .'/assets/js/highlight.js', array(), '1.0', true );
    wp_enqueue_script( 'app', get_stylesheet_directory_uri() .'/assets/js/app.js', array(), '1.0', true );
    wp_enqueue_script( 'script_main', get_stylesheet_directory_uri() .'/assets/js/script.js', array(), '1.0', true );
}
// Отключение родительских стилей
function wpschool_disable_scripts_styles() {
    wp_dequeue_style( 'twentytwenty-print-style-css' );
    wp_deregister_style('twentytwenty-print-style-css');
}
add_action( 'wp_enqueue_scripts', 'wpschool_disable_scripts_styles');

// Подключение класса Walker
include_once "class-wp-navwalker.php";

// Регистрация новых областей виджетов

add_action( 'widgets_init', 'register_widgets_child_theme' );
function register_widgets_child_theme(){
    register_sidebar( array(
        'name'          => sprintf(__('First menu footer'), 1 ),
        'id'            => "first-menu-footer",
    ) );
    register_sidebar( array(
        'name'          => sprintf(__('Second menu footer'), 1 ),
        'id'            => "second-menu-footer",
    ) );
    register_sidebar( array(
        'name'          => sprintf(__('Social menu footer'), 1 ),
        'id'            => "social-menu-footer",
    ) );
    register_sidebar( array(
        'name'          => sprintf(__('Copyright'), 1 ),
        'id'            => "copyright",
    ) );
    register_sidebar( array(
        'name'          => sprintf(__('Contacts footer'), 1 ),
        'id'            => "contacts-footer",
    ) );
    register_sidebar( array(
        'name'          => sprintf(__('Header text'), 1 ),
        'id'            => "header_text",
    ) );
    /*register_sidebar( array(
        'name'          => sprintf(__('Ajax posts menu'), 1 ),
        'id'            => "ajax_posts_menu",
    ) );*/
}

// Уаляем ненужные стандартные виджеты
add_action( 'widgets_init', 'remove_calendar_widget' );
function remove_calendar_widget() {
    unregister_widget('WP_Widget_Pages');            // Виджет страниц
    unregister_widget('WP_Widget_Calendar');         // Календарь
    unregister_widget('WP_Widget_Archives');         // Архивы
    unregister_widget('WP_Widget_Links');            // Ссылки
    unregister_widget('WP_Widget_Meta');             // Мета виджет
    unregister_widget('WP_Widget_Search');           // Поиск
    unregister_widget('WP_Widget_Categories');       // Категории
    unregister_widget('WP_Widget_Recent_Posts');     // Последние записи
    unregister_widget('WP_Widget_Recent_Comments');  // Последние комментарии
    unregister_widget('WP_Widget_RSS');              // RSS
    unregister_widget('WP_Widget_Tag_Cloud');        // Облако меток
    unregister_widget('WP_Widget_Media_Audio');      // Audio
    unregister_widget('WP_Widget_Media_Video');      // Video
    unregister_widget('WP_Widget_Media_Gallery');    // Gallery
    unregister_widget('WP_Widget_Media_Image');
}

// Добавляем файл шорткода
include_once 'shortcode_img.php';
include_once 'shortcode_video.php';

// Работа с СОБЫТИЯМИ

/*add_action('publish_post', 'email_friends');
function email_friends( $post_ID ){
    $friends = 'bob@example.org, susie@example.org';
    wp_mail( $friends, "sally's blog updated", 'I just put something on my blog: http://blog.example.com');

    return $post_ID;
}*/

/*add_action( 'wp_head', array('My_Class', 'my_static_method') );

class My_Class {
    public function __construct(){

        add_action( 'save_post', array( $this, 'my_public_method' ) );

        add_action( 'save_post', array( __CLASS__, 'my_static_method' ) );
    }

    public function my_public_method( $post_id ){
        echo 'This is my_public_method';
    }

    static function my_static_method( $post_id ){
        echo 'This is my_static_method';
    }
}
*/


// Работа  с Costomizer
include('customizer.php');

// Регистрация виджета
include('slogan_widget_class.php');
include('about_us_widget_class.php');



/*function my_sticky_option(){
    global $post;

    if( $post->post_type == 'films' && did_action('quick_edit_custom_box') === 1 ){
        ?>

        <fieldset class="inline-edit-col-right">
            <div class="inline-edit-col">
                <label class="alignleft">
                    <input type="checkbox" name="sticky" value="sticky" />
                    <span class="checkbox-title">
                                        <?php _e( 'Featured (sticky)', 'textdomain_string'); ?>
                                        Срабатывает только один раз
                                    </span>
                </label>
            </div>
        </fieldset>

        <?php
    }

}
add_action('quick_edit_custom_box', 'my_sticky_option');*/








