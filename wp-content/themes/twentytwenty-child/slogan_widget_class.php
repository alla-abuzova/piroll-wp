<?php
class Slogan_Widget extends WP_Widget{

    function __construct() {
        // Запускаем родительский класс
        parent::__construct(
            'id_slogan', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: my_widget
            'Slogan',
            array('description' => 'This widget enter a slogan into site header')
        );
    }

    public function widget( $args, $instance ) {

        $title = apply_filters( 'widget_title', $instance['title'] );
        $description = $instance['description'];
        $submit_name = $instance['submit_name'];

        echo $args['before_widget'];

        if( $description ){
            //echo $args['before_title'] . $title . $args['after_title'];

        ?>

            <div class="slogan col-6">
                <p class="slogan-title"><?php echo $title ?></p>
                <p class="slogan-description"><?php echo $description ?></p>
                <button><?php echo $submit_name ?></button>
            </div>

        <?php
        }

        echo $args['after_widget'];
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = "Пустое поле";
        }
        if ( isset( $instance[ 'description' ] ) ) {
            $description = $instance[ 'description' ];
        } else {
            $description = "Пустое поле";
        }
        if ( isset( $instance[ 'submit_name' ] ) ) {
            $submit_name = $instance[ 'submit_name' ];
        } else {
            $submit_name = "Пустое поле";
        }

    ?>

        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Заголовок</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <lable for="<?php echo $this->get_field_id('description');?>">Описание</lable>
            <input class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" type="text" value="<?php echo esc_attr( $description ); ?>" />
        </p>
        <p>
            <lable for="<?php echo $this->get_field_id('submit_name');?>">Описание</lable>
            <input class="widefat" id="<?php echo $this->get_field_id('submit_name'); ?>" name="<?php echo $this->get_field_name('submit_name'); ?>" type="text" value="<?php echo esc_attr( $submit_name ); ?>" />
        </p>
    <?php

    }

    public function update( $new_instance, $old_instance ) {

        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
        $instance['submit_name'] = ( ! empty( $new_instance['submit_name'] ) ) ? strip_tags( $new_instance['submit_name'] ) : '';
        return $instance;
    }
}

function register_base_widget1() {
    register_widget( 'Slogan_Widget' );
}
add_action( 'widgets_init', 'register_base_widget1' );