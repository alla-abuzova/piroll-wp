<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
<footer>
<!--<footer id="site-footer" role="contentinfo" class="header-footer-group">-->

   <!-- <div class="section-inner">

        <div class="footer-credits">

            <p class="footer-copyright">&copy;
                <?php
  //              echo date_i18n(
                /* translators: Copyright date format, see https://www.php.net/date */
  /*                  _x( 'Y', 'copyright date format', 'twentytwenty' )
                );
                ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
            </p><!-- .footer-copyright -->

            <p class="powered-by-wordpress">
                <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentytwenty' ) ); ?>">
                    <?php _e( 'Powered by WordPress', 'twentytwenty' ); ?>
                </a>
            </p><!-- .powered-by-wordpress -->

        </div><!-- .footer-credits -->

        <a class="to-the-top" href="#site-header">
						<span class="to-the-top-long">
							<?php
                            /* translators: %s: HTML character for up arrow. */
    /*                        printf( __( 'To the top %s', 'twentytwenty' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' );
                            ?>
						</span><!-- .to-the-top-long -->
            <span class="to-the-top-short">
							<?php
                            /* translators: %s: HTML character for up arrow. */
     /*                       printf( __( 'Up %s', 'twentytwenty' ), '<span class="arrow" aria-hidden="true">&uarr;</span>' );
     */                       ?>
	<!--					</span><!-- .to-the-top-short -->
    <!--    </a><!-- .to-the-top -->

    <!--</div><!-- .section-inner -->




    <style>
        body footer .widget_nav_menu, body footer .widget_custom_html{
            list-style: none !important;
            margin-top: -10px;
        }
        body footer .container a{
            text-decoration: none;
        }
        body footer .container a:hover{
            text-decoration: underline;;
        }
    </style>
    <div class="container footer-cont">
        <div class="col-s-6 col-6">
            <?php if ( is_active_sidebar( 'copyright' ) ) : ?>
                <?php dynamic_sidebar( 'copyright' ); ?>
            <?php endif; ?>
            <!--<p>Pirol Design, Inc.</p>
            <p>@ 2017 Pirol. All rights reserved.<br />
                Designed by robirurk</p>-->
        </div>
        <div class="col-s-6 col-6">
            <?php if ( is_active_sidebar( 'contacts-footer' ) ) : ?>
                <?php dynamic_sidebar( 'contacts-footer' ); ?>
            <?php endif; ?>
            <!--<p>hello@alla@com.ua</p>
            <p><a href="80990000000">+80-99-000-00-00</a></p>-->
        </div>
    </div>
    <div class="container footer-menu">
        <div class="col-s-6 col-4">
            <?php if ( is_active_sidebar( 'first-menu-footer' ) ) : ?>
                <?php dynamic_sidebar( 'first-menu-footer' ); ?>
            <?php endif; ?>
        </div>
        <div class="col-s-6 col-4">
            <?php if ( is_active_sidebar( 'second-menu-footer' ) ) : ?>
                <?php dynamic_sidebar( 'second-menu-footer' ); ?>
            <?php endif; ?>
        </div>
        <div class="col-s-6 col-4">
            <?php if ( is_active_sidebar( 'social-menu-footer' ) ) : ?>
                <?php dynamic_sidebar( 'social-menu-footer' ); ?>
            <?php endif; ?>
        </div>
    </div>
</footer><!-- #site-footer -->


<?php wp_footer(); ?>

</body>
</html>
