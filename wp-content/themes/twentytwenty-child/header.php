<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

<head>

    <meta charset="<?php //bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >

    <link rel="profile" href="https://gmpg.org/xfn/11">


    <?php
       // wp_deregister_script('jquery');

    ?>


    <?php wp_head(); ?>
    <!--<script src="/wp-content/themes/twentytwenty-child/assets/js/jquery.min.js"></script>
    <script src="/wp-content/themes/twentytwenty-child/assets/js/owl.carousel.min.js"></script>-->

</head>

<body <?php //body_class(); ?>>

<?php
//wp_body_open();
?>
<header class="clearfix" id="home">
<!--<header id="site-header" class="header-footer-group" role="banner">-->

    <!--<div class="header-inner section-inner">

        <div class="header-titles-wrapper">-->

            <?php

            // Check whether the header search is activated in the customizer.
           /* $enable_header_search = get_theme_mod( 'enable_header_search', true );

            if ( true === $enable_header_search ) {

                ?>

                <button class="toggle search-toggle mobile-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
							<span class="toggle-inner">
								<span class="toggle-icon">
									<?php twentytwenty_the_theme_svg( 'search' ); ?>
								</span>
								<span class="toggle-text"><?php _e( 'Search', 'twentytwenty' ); ?></span>
							</span>
                </button><!-- .search-toggle -->

            <?php } ?>

            <div class="header-titles">

                <?php
                // Site title or logo.
                twentytwenty_site_logo();

                // Site description.
                twentytwenty_site_description();
                ?>

            </div><!-- .header-titles -->

            <button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
						<span class="toggle-inner">
							<span class="toggle-icon">
								<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
							</span>
							<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
						</span>
            </button><!-- .nav-toggle -->

        </div><!-- .header-titles-wrapper -->

        <div class="header-navigation-wrapper">

            <?php
            if ( has_nav_menu( 'primary' ) || ! has_nav_menu( 'expanded' ) ) {
                ?>

                <nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e( 'Horizontal', 'twentytwenty' ); ?>" role="navigation">

                    <ul class="primary-menu reset-list-style">

                        <?php
                        if ( has_nav_menu( 'primary' ) ) {

                            wp_nav_menu(
                                array(
                                    'container'  => '',
                                    'items_wrap' => '%3$s',
                                    'theme_location' => 'primary',
                                )
                            );

                        } elseif ( ! has_nav_menu( 'expanded' ) ) {

                            wp_list_pages(
                                array(
                                    'match_menu_classes' => true,
                                    'show_sub_menu_icons' => true,
                                    'title_li' => false,
                                    'walker'   => new TwentyTwenty_Walker_Page(),
                                )
                            );

                        }
                        ?>

                    </ul>

                </nav><!-- .primary-menu-wrapper -->

                <?php
            }

            if ( true === $enable_header_search || has_nav_menu( 'expanded' ) ) {
                ?>

                <div class="header-toggles hide-no-js">

                    <?php
                    if ( has_nav_menu( 'expanded' ) ) {
                        ?>

                        <div class="toggle-wrapper nav-toggle-wrapper has-expanded-menu">

                            <button class="toggle nav-toggle desktop-nav-toggle" data-toggle-target=".menu-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
									<span class="toggle-inner">
										<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
										<span class="toggle-icon">
											<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
										</span>
									</span>
                            </button><!-- .nav-toggle -->

                        </div><!-- .nav-toggle-wrapper -->

                        <?php
                    }

                    if ( true === $enable_header_search ) {
                        ?>

                        <div class="toggle-wrapper search-toggle-wrapper">

                            <button class="toggle search-toggle desktop-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
									<span class="toggle-inner">
										<?php twentytwenty_the_theme_svg( 'search' ); ?>
										<span class="toggle-text"><?php _e( 'Search', 'twentytwenty' ); ?></span>
									</span>
                            </button><!-- .search-toggle -->

                        </div>

                        <?php
                    }
                    ?>

                </div><!-- .header-toggles -->
                <?php
            }
            ?>

        </div><!-- .header-navigation-wrapper -->

    </div><!-- .header-inner -->

    <?php
    // Output the search modal (if it is activated in the customizer).
    if ( true === $enable_header_search ) {
        get_template_part( 'template-parts/modal-search' );
    }*/
    ?>




        <div class="clearfix header-fixed" id="fix-menu">
            <div class="container-menu">
                <!--<a href="#"><img class="logo" src="<?php //echo get_stylesheet_directory_uri()?>/assets/images/logo.png" alt=""></a>-->
                <?php
                // получаем ссылку на логотип
                $custom_logo__url = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' );
                // выводим
                echo '<a href="' . get_site_url() . '"><img class="logo" src="' .    $custom_logo__url[0] . '" alt=""></a>';

                ?>
                <?php
                /*wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'container_class' => 'hamburger-menu'
                    )
                );*/
                $args = array(
                    'theme_location' => 'primary',
                    // 'walker'=> new NawWalker() // этот параметр нужно добавить
                    'container'       => 'div',
                    'container_class' => 'hamburger-menu',
                    'container_id'    => '',
                    'menu_class'      => 'menu__box',
                    'menu_id'         => 'menu',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<input id=\'menu__toggle\' type=\'checkbox\'/>
                    <lable class=\'menu__btn\' for=\'menu__toggle\'>
                        <span></span>
                    </lable><ul id="%1$s" class="%2$s">%3$s</ul>',
                    'item_spacing'    => 'preserve',
                    'depth'           => 1,
                    'walker'          => new NawWalker(),
                );
                wp_nav_menu( $args );


                /* add_filter( 'wp_nav_menu', 'change_wp_nav_menu', 10, 2 );

                 function change_wp_nav_menu( $nav_menu, $args ) {
                     return '<section class="menu-wrap">' . $nav_menu . '</section>';
                 }*/

                ?>


                <!--<div class="hamburger-menu">
                    <input id="menu__toggle" type="checkbox"/>
                    <lable class="menu__btn" for="menu__toggle">
                        <span></span>
                    </lable>
                    <ul class="menu__box" id="menu">
                        <li><a class="menu__item" href="#home">home</a></li>
                        <li><a class="menu__item" href="#about-us" >about</a></li>
                        <li><a class="menu__item" href="#work">work</a></li>
                        <li><a class="menu__item" href="#process">process</a></li>
                        <li><a class="menu__item" href="#services">services</a></li>
                        <li><a class="menu__item" href="#testimonials">testimonials</a></li>
                        <li><a class="menu__item" href="#contact">contact</a></li>
                    </ul>
                </div>-->
            </div>
        </div>
    <style>
        body .slogan{
            list-style: none !important;
        }
    </style>
        <div class="container" id="home">
            <div class="slogan col-6">
                <?php if ( is_active_sidebar( 'header_text' ) ) : ?>
                    <?php dynamic_sidebar( 'header_text' ); ?>
                <?php endif; ?>

               <!-- <p class="slogan-title">We design and develop</p>
                <p class="slogan-description">Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую кол</p>
                <button>contact us</button><br/>-->
                <?php //do_action('customize_register');
                //echo get_template_part('breadcrumbs');


                //post_type_archive_title( 'Это новые посты', true );

                ?>
                

                <p><?php




                  /*
                  echo
                   add_filter( 'publish_post', 'email_friends' );*/

                /*  $var = new My_Class();
                do_action('wp_head');
                do_action('save_post', $var);
                echo did_action('save_post');
                if (did_action('wp_head') === 2)
                    echo 'Выполнено 2 раза';



                do_action('quick_edit_custom_box'); */


                    ?></p>
            </div>
        </div>
</header><!-- #site-header -->

<?php
// Output the menu modal.
//get_template_part( 'template-parts/modal-menu' );