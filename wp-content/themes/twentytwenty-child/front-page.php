<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

    <main id="site-content" role="main">

        <?php if ( is_active_sidebar( 'ajax_posts_menu' ) ) : ?>
            <?php dynamic_sidebar( 'ajax_posts_menu' ); ?>
        <?php endif; ?>

        <?php

        if ( have_posts() ) {

            $i = 0;

            while ( have_posts() ) {
                $i++;
                if ( $i > 1 ) {
                    echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
                }
                the_post();

                get_template_part( 'template-parts/content', get_post_type() );

            }
        } elseif ( is_search() ) {
            ?>

            <div class="no-search-results-form section-inner thin">

                <?php
                get_search_form(
                    array(
                        'label' => __( 'search again', 'twentytwenty' ),
                    )
                );
                ?>

            </div><!-- .no-search-results -->

            <?php
        }
        ?>

        <?php get_template_part( 'template-parts/pagination' );
     ?>

    </main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

    <section class="reviews" id="testimonials">
        <div class="container">
            <section id="demos">
                <div class="row">
                    <div class="large-12 columns">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <p class="reviews-description">&#8220;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.&#8221;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</p>
                                <p class="reviews-author">michael hopkins</p>
                            </div>
                            <div class="item">
                                <p class="reviews-description">&#8220;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.&#8221;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</p>
                                <p class="reviews-author">michael hopkins</p>
                            </div>
                            <div class="item">
                                <p class="reviews-description">&#8220;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.&#8221;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</p>
                                <p class="reviews-author">michael hopkins</p>
                            </div>
                            <div class="item">
                                <p class="reviews-description">&#8220;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.&#8221;Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</p>
                                <p class="reviews-author">michael hopkins</p>
                            </div>
                        </div>
                        <script>
                            jQuery(document).ready(function($) {
                                let owl = $('.owl-carousel');
                                owl.owlCarousel({
                                    margin: 10,
                                    nav: false,
                                    loop: true,
                                    responsive: {
                                        0: {
                                            items: 1
                                        },
                                        600: {
                                            items: 1
                                        },
                                        1000: {
                                            items: 1
                                        }
                                    }
                                });
                            });
                        </script>
                    </div>
                </div>
            </section>

        </div>
    </section>


<?php do_action('quiz_loc'); ?>
<?php //do_action('rest_api'); ?>




<?php
get_footer();