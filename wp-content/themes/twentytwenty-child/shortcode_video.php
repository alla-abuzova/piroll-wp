<?php

add_shortcode('video', 'videoFun');
function videoFun( $atts, $url_video ){
    $url = '<video src="' . get_stylesheet_directory_uri() . $url_video . '" controls></video>';
    return $url;
}

?>