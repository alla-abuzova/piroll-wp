<?php

require_once plugin_dir_path(__FILE__) . 'class-quiz-basic-metabox.php';

class QuestionMetabox extends BasicMetabox{
    public function __constract($post_id){
        parent::__construct($post_id);
    }

    public  function save( $post_id ) {

        $slug = 'quiz';

        if ( $slug != $_POST['post_type'] )
            return;

        if ( isset( $_REQUEST['question'] ) ) {
            update_post_meta( $post_id, 'question', sanitize_text_field( $_REQUEST['question'] ) );
        }
    }

    public function draw($post)
    {
        $data = get_post_meta( $post->ID, 'question', true );
        ?>
        <form>
            <div >
                <label for="question">Вопрос №1 </label><textarea rows="2" cols="60" name="question" id=""><?php echo $data; ?></textarea>
            </div>
        </form>
        <?php
    }

    public function add_mb(){
        add_meta_box( 'id_meta_quest_m','Поле для ввода вопроса',[$this, 'draw'], 'quiz' );
    }

    public function __destruct() {
        parent::__destruct();
    }
}