<?php



abstract class BasicMetabox{

    public $post_id;

    public function __construct($post_id) {
        add_action('add_meta_boxes', [$this, 'add_mb']);
        add_action( 'save_post', [$this, 'save']);
    }
    protected abstract function save($post_id);
    protected abstract function draw($post);
    protected abstract function add_mb();

    public function __destruct() {
    }

}