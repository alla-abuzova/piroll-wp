<?php

class Quiz_Post_Type {
    function __construct() {
        add_action('init', [$this, 'register']);
    }

    public function register(){
        register_post_type('quiz', array(
            'labels'             => array(
                'name'               => 'Тесты',
                'singular_name'      => 'Тест',
                'add_new'            => 'Добавить новую',
                'add_new_item'       => 'Добавить новый тест',
                'edit_item'          => 'Редактировать тест',
                'new_item'           => 'Новая тест',
                'view_item'          => 'Посмотреть тест',
                'search_items'       => 'Найти тест',
                'not_found'          =>  'Тестов не найдено',
                'not_found_in_trash' => 'В корзине тестов не найдено',
                'parent_item_colon'  => '',
                'menu_name'          => 'Тесты'

            ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => true,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array('title','editor','author','thumbnail','excerpt','comments')
        ) );
    }

}