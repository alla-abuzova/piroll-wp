
<?php

// Вывод оповещения об успешной отправки запроса
add_action( 'test_data' , 'restFuncTest');

function restFuncTest(){
    $url = 'http://piroll-wp/wp-content/plugins/quiz-constructor/quiz-successful-request.php/get?a=b&c=d';


    $args = array(
        'timeout' => 5, // Время в секундах на получение данных.
        'user-agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
    );

    $response = wp_remote_get( $url, $args );
    $body = wp_remote_retrieve_body( $response );

    print_r( $body );

      //echo 'Hello it is rest API';
}