<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Quiz Constructor
 *
 * @wordpress-plugin
 * Plugin Name:       Quiz Constructor
 * Description:       WordPress Plugin Quiz Constructor. This plugun allows make own quiz with any theme.
 * Version:           1.0.0
 * Author:            Alla Abuzova
 * Text Domain:       quiz-constructor
 */


require_once plugin_dir_path(__FILE__) . 'includes/class-quiz-activator.php';
require_once plugin_dir_path(__FILE__) . 'includes/class-quiz-deactivator.php';
require_once plugin_dir_path(__FILE__) . 'includes/class-quiz-constructor.php';
require_once plugin_dir_path(__FILE__) . 'includes/metabox/class-quiz-question-metabox.php';


$quiz_constr = new QuizConstructor();
$post_id = get_the_ID();
$metaboxQuestion = new QuestionMetabox($post_id);

add_action('quiz_loc', 'data_output');


function data_output()
{
    ?>
    <form action="/wp-content/plugins/quiz-constructor/includes/quiz-successful-request.php" method="post">
        <h2>1. Вопрос №1</h2>
        <p>
            <input type="checkbox" id="check1" name="check1" value="Вариант ответа №1"><label for="check1">Вариант ответа №1</label>
        </p>
        <p>
            <input type="checkbox" id="check2" name="check2" value="Вариант ответа №2"><label for="check2">Вариант ответа №2</label>
        </p>
        <p>
            <input type="checkbox" id="check3" name="check3" value="Вариант ответа №3"><label for="check3">Вариант ответа №3</label>
        </p>
        <button>Проверить</button>
    </form>

    <?php
}