<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'piroll_wp_gith' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'N+^Kb:0S5?#`~eIE!HJpu=,JMX{C${($>+HKj.$%#H{@/V6f{G` 3(.>ia,zy#?7' );
define( 'SECURE_AUTH_KEY',  '9Ad3B_tNMi{EN6:z:4md(>60^h-(l6snfSo!}VEnaqI<6{vaCW_^kmLl^8`>,Hv&' );
define( 'LOGGED_IN_KEY',    'a~Ks(Gqps-pEE9jH^T5KD{V6{?{^v`qv8<wCL3{iCU*~~Kl%u8Yc(M#tRkN[yE(r' );
define( 'NONCE_KEY',        '&t]*=y1N-A$D:Z<4egXug$Zj-S*d#.xIrJ}?qiw6C*PqF^#<O(l`[2evd&B~RjWe' );
define( 'AUTH_SALT',        '+GR<rik6DV9}w2[,:`De`?[QQ!x(eq*G2>8kIJ.og#%1-VFD96$zA)vB0&`}s]8Q' );
define( 'SECURE_AUTH_SALT', '#%LEEcp`X%n)lIB+T(#kIlXXZgonqDXHzDb4Y<QvCX{JQXJPneQ7uX )DWhBSg_u' );
define( 'LOGGED_IN_SALT',   '%E<D%D0,cy=S_#1L},)Ar^n167JtlB2U+J&HUC+462`C;:c8G92~F+pUD6/Q/g7p' );
define( 'NONCE_SALT',       'k:XVE^::r)4;ZS7[!$;^c4`D=Uy}1Kbr%&DmSqzVAzqt[U{bs l~1uZ>)A,_DF|I' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define( 'WP_DEBUG', false );
define( 'WP_DEBUG', true );
define( 'SAVEQUERIES', true );


/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
